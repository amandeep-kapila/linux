# Linux

Linux OS VMs, fresh and customized....

Linux Flavour: Debian 10, Buster

Basics: Bare-bone server | without any GUI | LVM | RAM 1Gigs | HDD 32Gigs

Platform: OracleVirtual Box

Passwords:
root/root123

Non-root user/password:
dbadmin/dbadmin123 (Sudo enabled)

Network Configuration:
Local,
CIDR 192.168.0.0/24